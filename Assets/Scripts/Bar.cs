﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Role
{
    Developer,
    Artist,
    Music,
    None
}

public class Bar : MonoBehaviour
{
    [HideInInspector]
    public Color currRoleColor;

    [Space]

    public Color staminaColorA;
    public Color staminaColorB;

    public Transform staminaTrans;

    [Space]

    public SpriteRenderer staminaSprite;
    public SpriteRenderer roleSprite;

    public Human human;

    void Start()
    {
        if (!human)
        {
            human = GetComponentInParent<Human>();
        }
    }

    void UpdateRoleColor()
    {
        Role role = Role.None;

        if (human)
        {
            role = human.role;
        }

        switch (role)
        {
            case Role.Artist:
                currRoleColor = GameManager.main.artistColor;
                break;
            case Role.Developer:
                currRoleColor = GameManager.main.developerColor;
                break;
            case Role.Music:
                currRoleColor = GameManager.main.musicColor;
                break;
            default:
                currRoleColor = Color.gray;
                break;
        }

        roleSprite.color = currRoleColor;
    }

    void Update()
    {
        float staminaValue = 1;

        if (human)
        {
            staminaValue = human.currStamina / 100f;

            staminaValue = Mathf.Clamp01(staminaValue);
        }

        UpdateRoleColor();

        currRoleColor = Color.Lerp(staminaColorB, staminaColorA, staminaValue);

        staminaSprite.color = currRoleColor;

        staminaTrans.localScale = new Vector3(1, staminaValue, 1);
    }
}
