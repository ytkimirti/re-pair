﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public float fixedWidth;
    public Camera cam;

    public static CameraController main;

    private void Awake()
    {
        main = this;
    }

    void Start()
    {

    }

    private void OnValidate()
    {
        UpdateCamSize();
    }

    void UpdateCamSize()
    {
        cam.orthographicSize = fixedWidth * (1 / cam.aspect);

    }

    void Update()
    {
        UpdateCamSize();
    }


}
