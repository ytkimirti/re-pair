﻿using UnityEngine;
using TMPro;
using DG.Tweening;

public class Clock : MonoBehaviour
{
    public float gameDuration = 160F;
    public TextMeshPro timeText;

    public float currTime;

    int memTime;

    // Start is called before the first frame update
    void Start()
    {

    }

    void PunchPos()
    {
        transform.DOShakePosition(0.12f, 0.25f);
    }

    // Update is called once per frame
    void Update()
    {
        float gameTime = Time.time / gameDuration;
        currTime = gameTime * 48F;

        int intTime = Mathf.RoundToInt(currTime);

        if (intTime != memTime)
        {
            PunchPos();
        }

        timeText.text = intTime.ToString() + ":00";

        memTime = intTime;
    }
}
