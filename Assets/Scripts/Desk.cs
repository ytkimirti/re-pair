﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Desk : MonoBehaviour
{
    public Chair[] currChairs;

    public float damage;
    public float heal;

    public float cycleTime;
    float timer;

    public Transform roadTrans;

    public Notif notif;

    public Transform roadTransUp, roadTransDown;

    int numberOfHuman;

    public int score;

    void Start()
    {
        roadTrans = transform.position.y < 0 ? roadTransUp : roadTransDown;

        LookForChairs();
    }

    void LookForChairs()
    {
        currChairs = GetComponentsInChildren<Chair>();
    }

    int CalculateTeamPoint()
    {
        List<Role> roles = new List<Role>();

        numberOfHuman = 0;

        for (int i = 0; i < currChairs.Length; i++)
        {
            if (currChairs[i].currHuman)
            {
                numberOfHuman++;
                roles.Add(currChairs[i].currHuman.role);
            }
        }

        //Get the score


        int numberOfDifferentRoles = 0;

        if (roles.Contains(Role.Artist))
        {
            numberOfDifferentRoles++;
        }

        if (roles.Contains(Role.Developer))
        {
            numberOfDifferentRoles++;
        }
        if (roles.Contains(Role.Music))
        {
            numberOfDifferentRoles++;
        }

        score = numberOfDifferentRoles;

        return numberOfDifferentRoles;
    }

    void Update()
    {
        timer += Time.deltaTime;

        if (timer > cycleTime)
        {
            timer = 0;

            float change = 0;

            GameManager.main.OnDeskCycle();

            int point = CalculateTeamPoint();

            if (numberOfHuman > 0)
            {
                if (point < 2)
                {
                    change = damage;

                    notif.Notiffff("Team not suitable!", Color.red);
                }
                else if (point > 2)
                {
                    change = heal;

                    notif.Notiffff("+Development", Color.green);
                }
                else
                {
                    notif.Notiffff("Balanced", Color.yellow);
                }
            }

            foreach (Chair chair in currChairs)
            {
                if (chair.currHuman)
                {
                    chair.currHuman.ChangeStamina(change);
                }
            }
        }
    }


    public Chair FindOpenChair()
    {
        Chair openChair = null;

        foreach (Chair chair in currChairs)
        {
            if (chair.status == "open")
            {
                openChair = chair;
                break;
            }
        }

        return openChair;
    }
}
