﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class JammerInput : MonoBehaviour
{
    public Vector2 randomEventTime;
    public float randomEventChance;

    DialogBox dialog;
    public Human human;

    void Start()
    {
        human = GetComponent<Human>();

        dialog = GetComponentInChildren<DialogBox>();

        StartCoroutine(JammerEnum());
    }

    void SitOnRandomChair()
    {
        Chair najsChair = GameManager.main.FindAvailableRandomChair();

        if (najsChair)
            human.ChaseChair(najsChair);
    }

    void SitOnAnyChair()
    {
        Chair najsChair = GameManager.main.FindAvailableChair();

        if (najsChair)
            human.ChaseChair(najsChair);
    }


    IEnumerator JammerEnum()
    {
        yield return new WaitForSeconds(Random.Range(0, 2f));

        SitOnAnyChair();

        while (true)
        {
            yield return new WaitForSeconds(Random.Range(randomEventTime.x, randomEventTime.y));

            if (M.Change(randomEventChance))
            {
                RandomEvent();
            }

            //itOnRandomChair();
        }
    }

    [Button]
    public void RandomEvent()
    {
        int randNum = Random.Range(0, 100);

        if (randNum < 50)
        {
            RandomTalk();
        }
        else if (randNum < 80)
        {
            SitRandomChairEvent();
        }
        else
        {
            StartCoroutine("puke");
        }
    }

    public void RandomTalk()
    {
        dialog.StartDialog(GameManager.main.GetRandomDialog("random"));
    }

    [Button]
    public void SitRandomChairEvent()
    {
        dialog.StartDialog(GameManager.main.GetRandomDialog("change_desk"));

        SitOnRandomChair();
    }

    public void CororoutineStart(string name)
    {
        StartCoroutine(name);
    }

    public IEnumerator puke()
    {

        dialog.StartDialog(GameManager.main.GetRandomDialog("puke"));

        human.GoTo(Vector2.right * 100);

        human.invincible = true;

        yield return new WaitForSeconds(10f);

        human.invincible = false;

        SitOnAnyChair();
    }

    public IEnumerator internet_gone()
    {
        if (M.Change(30))
        {
            dialog.StartDialog(GameManager.main.GetRandomDialog("internet_gone"));
        }

        human.GoTo(GameManager.main.RandomSpawnPoint());

        yield return new WaitForSeconds(5f);

        SitOnAnyChair();
    }



    void Update()
    {

    }
}
