﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NodeFollowerInput : MonoBehaviour
{
    public Transform currTarget;
    public Transform debugTrans;

    public Vector2[] currPath;

    public float minDistance;

    int currPathID;

    Human human;

    public Action OnTargetAchieved;

    void Start()
    {
        human = GetComponent<Human>();
    }

    void Update()
    {
        MoveToTarget();
    }

    public void Move(Vector2 target)
    {
        currPath = ZoneManager.main.FindPath(transform.position, target);
    }

    void MoveToTarget()
    {
        Vector2 targetPos = Vector2.zero;

        if (currTarget)
        {
            targetPos = currTarget.position;
        }
        else
        {
            if (currPathID < currPath.Length)
            {
                targetPos = currPath[currPathID];
            }
        }


        //If there is a node to follow
        if (targetPos != Vector2.zero)
        {
            Vector2 localVector = targetPos - (Vector2)transform.position;

            human.directionInput = localVector.normalized;

            //Harsh distance calculation, doesnt have to be percıse so hehe
            if (Mathf.Abs(localVector.x) + Mathf.Abs(localVector.y) < minDistance)
            {
                //Get to the next node
                currPathID++;

                if (currPathID >= currPath.Length)
                {
                    //You have achieved the LAST node
                    ClearMovement();

                    OnTargetAchieved?.Invoke();
                }

                if (currTarget)
                {
                    //You have achieved the target transform
                    ClearMovement();

                    OnTargetAchieved?.Invoke();
                }
            }
        }
        else
        {
            human.directionInput = Vector2.zero;
        }
    }

    public void ClearMovement()
    {
        currPath = new Vector2[0];
        currPathID = 0;

        currTarget = null;
    }

    private void OnDrawGizmos()
    {
        if (currPath.Length == 0)
            return;

        Vector2 lastPos = currPath[0];

        Gizmos.color = Color.white;

        for (int i = 0; i < currPath.Length; i++)
        {
            Gizmos.DrawLine(lastPos, currPath[i]);
            Gizmos.DrawWireSphere(currPath[i], 0.1f);

            lastPos = currPath[i];
        }

        Gizmos.color = Color.yellow;

        Gizmos.DrawLine(transform.position, currPath[currPathID]);
    }
}
