﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class DialogBox : MonoBehaviour
{
    public GameObject dialogParent;
    public GameObject bubblePoint;

    public Text nameText;
    public Text sentenceText;

    // Start is called before the first frame update
    void Start()
    {
        dialogParent.SetActive(false);
        bubblePoint.SetActive(false);
    }

    void Update()
    {

    }

    public void StartDialog(string sentence)
    {
        StopAllCoroutines();
        StartCoroutine(typeSentence(sentence));
    }

    IEnumerator typeSentence(string sentence)
    {
        dialogParent.SetActive(true);
        bubblePoint.SetActive(true);

        sentenceText.text = "";

        foreach (char letter in sentence.ToCharArray())
        {
            sentenceText.text += letter;
            yield return new WaitForSeconds(0.05f);
        }

        yield return new WaitForSeconds(3);

        sentenceText.text = "";

        dialogParent.SetActive(false);
        bubblePoint.SetActive(false);
    }

}
