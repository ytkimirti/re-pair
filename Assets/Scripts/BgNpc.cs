﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BgNpc : MonoBehaviour
{
    public Transform[] pathTrans;

    public Vector2 nodeRandWait;

    int currID;

    public Human human;

    void Start()
    {
        StartCoroutine(moveLoop());
    }

    void Update()
    {

    }

    IEnumerator moveLoop()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(nodeRandWait.x, nodeRandWait.y));

            currID++;

            if (currID >= pathTrans.Length)
            {
                currID = 0;
            }

            human.GoTo(pathTrans[currID].position);

        }
    }
}
