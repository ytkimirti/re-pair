﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class SpritePart
{
    public string name;
    public Sprite[] sprites;
}

[System.Serializable]
public class FacialExp
{
    public string name;
    public Sprite frontSprite;
    public Sprite sideSprite;
}

public class VisualController : MonoBehaviour
{
    [Header("INPUT")]

    public Vector2 directionVector;
    public char facing;

    [Header("References")]

    public SpriteRenderer faceRen;
    public SpriteRenderer headRen;

    public SpriteRenderer hairRen;
    public SpriteRenderer bodyRen;
    public SpriteRenderer[] handRen;
    public SpriteRenderer[] footRen;

    [Space]

    public Transform humanoidTrans;

    [Space]

    public bool staticSkin;

    public int bodyID, handID, footID, hairID;

    [Space]

    FacialExp currFacialExp;

    [Space]

    SpritePart bodyP;
    SpritePart handP;
    SpritePart footP;
    SpritePart hairP;

    public Animator anim;

    char memFacing = 'Y';

    void Start()
    {
        if (!staticSkin)
            SetRandomSkin();

        LoadSpritesFromGameManager();

        anim = GetComponentInChildren<Animator>();
    }

    void SetSecondsActive(bool active)
    {
        handRen[1].enabled = active;

        //Foots are always enabled
        //footRen[1].enabled = active;
    }

    public void SetRandomSkin()
    {
        bodyID = Random.Range(0, GameManager.main.bodyParts.Length);
        handID = Random.Range(0, GameManager.main.handParts.Length);
        footID = Random.Range(0, GameManager.main.footParts.Length);
        hairID = Random.Range(0, GameManager.main.hairParts.Length);
    }

    void SetHandArms()
    {
        SetSprites(handRen, handP.sprites[0]);
        SetSprites(footRen, footP.sprites[0]);
    }

    public void SetFacings(char facing)
    {
        SetHandArms();

        if (facing == 'L')
        {
            humanoidTrans.localScale = new Vector3(-Mathf.Abs(humanoidTrans.localScale.x), humanoidTrans.localScale.y, humanoidTrans.localScale.z);
        }
        else
        {
            humanoidTrans.localScale = new Vector3(Mathf.Abs(humanoidTrans.localScale.x), humanoidTrans.localScale.y, humanoidTrans.localScale.z);
        }

        if (facing == 'U')
        {
            faceRen.enabled = false;

            bodyRen.sprite = bodyP.sprites[2];
            hairRen.sprite = hairP.sprites[2];

            SetSecondsActive(true);
        }
        else if (facing == 'D')
        {
            faceRen.enabled = true;

            bodyRen.sprite = bodyP.sprites[0];
            hairRen.sprite = hairP.sprites[0];

            faceRen.sprite = currFacialExp.frontSprite;

            SetSecondsActive(true);
        }
        else
        {
            faceRen.enabled = true;

            bodyRen.sprite = bodyP.sprites[1];
            hairRen.sprite = hairP.sprites[1];

            faceRen.sprite = currFacialExp.sideSprite;

            SetSecondsActive(false);
        }
    }

    void SetSprites(SpriteRenderer[] renderers, Sprite sprite)
    {
        foreach (SpriteRenderer rend in renderers)
        {
            rend.sprite = sprite;
        }
    }

    void LoadSpritesFromGameManager()
    {
        GameManager man = GameManager.main;

        bodyP = man.bodyParts[bodyID];
        handP = man.handParts[handID];
        footP = man.footParts[footID];
        hairP = man.hairParts[hairID];

        currFacialExp = man.facialExps[0];
    }

    void UpdateAnimatorParemeters()
    {
        anim.SetBool("isMoving", directionVector != Vector2.zero);

        anim.SetBool("isVertical", facing != 'R' && facing != 'L');

        anim.SetBool("isRight", facing == 'R');

    }

    void Update()
    {
        if (memFacing != facing)
            SetFacings(facing);

        memFacing = facing;

        UpdateAnimatorParemeters();

        //Faces
    }
}
