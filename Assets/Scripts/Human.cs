﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class Human : MonoBehaviour
{
    [Header("Current Status")]


    public bool isDed = false;
    public bool isHolded = false;
    public char currFacing;

    public float currStamina = 100;

    public bool invincible = false;

    [Header("Configure")]

    public float staminaLostBySecond;

    public Role role;

    [Header("INPUT")]

    public Vector2 directionInput;

    [Header("Desks")]

    //This represents the current SITTING chair
    public Chair currChair;
    public Chair targetChair;

    [Header("Movement")]

    public float movementSpeed;
    public float accelSpeed;

    Rigidbody2D rb;
    public NodeFollowerInput nodeFollower;
    VisualController visual;
    Notif notif;
    public DialogBox dialogBox;

    void Start()
    {
        dialogBox = GetComponentInChildren<DialogBox>();
        notif = GetComponentInChildren<Notif>();
        visual = GetComponentInChildren<VisualController>();
        nodeFollower = GetComponent<NodeFollowerInput>();
        rb = GetComponent<Rigidbody2D>();

        if (nodeFollower)
        {
            nodeFollower.OnTargetAchieved += OnTargetAchieved;
        }
    }

    private void Update()
    {
        if (false)
        {
            Chair[] chairs = FindObjectsOfType<Chair>();

            ChaseChair(chairs[Random.Range(0, chairs.Length)]);
        }

        //If we have a target chair, but if thats occupied, cancel the mission
        if (targetChair && targetChair.currHuman)
        {
            CancelChairFollow();
        }

        visual.facing = currFacing;
        visual.directionVector = directionInput;
    }

    public void ChangeStamina(float change, string message)
    {
        if (change != 0)
        {
            bool isPositive = change > 0;

            Color col = isPositive ? Color.green : Color.red;

            string startChar = isPositive ? "+" : "-";

            notif.Notiffff(message + " " + startChar + Mathf.RoundToInt(Mathf.Abs(change)).ToString(), col);
        }

        //Other

        currStamina += change;

        CheckStamina();
    }

    public void ChangeStamina(float change)
    {
        if (change != 0)
        {
            bool isPositive = change > 0;

            Color col = isPositive ? Color.green : Color.red;

            string startChar = isPositive ? "+" : "-";

            notif.Notiffff(startChar + Mathf.RoundToInt(Mathf.Abs(change)).ToString(), col);
        }

        //Other

        currStamina += change;

        CheckStamina();
    }

    public void CheckStamina()
    {
        if (currStamina <= 0)
        {
            currStamina = 0;
            Die();
        }
    }

    void LooseStaminaOverTime()
    {
        currStamina -= Time.deltaTime * staminaLostBySecond;

        CheckStamina();
    }

    public void Die()
    {
        if (isDed)
        {
            return;
        }

        CameraShaker.Instance.ShakeOnce(3, 7, 0, 1);

        isDed = true;
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;

        ParticleManager.main.play(transform.position, Vector3.zero, 0);

        Destroy(gameObject);
    }

    void FixedUpdate()
    {
        rb.isKinematic = isHolded || currChair;

        Move(directionInput);

        if (!currChair)
        {
            //This means if there IS an input and we can find direction from it
            if (FindFacingFromInput() != 'N')
            {
                currFacing = FindFacingFromInput();
            }
        }
        else
        {
            //If there IS a chair, our facing will be equal to its.
            currFacing = currChair.currFacing;
        }

        isHolded = false;
    }

    public void GetHolded()
    {
        isHolded = true;

        currChair = null;

        currFacing = 'D';

        visual.anim.SetTrigger("Holded");

        CancelChairFollow();
    }

    public void GetDropped()
    {
        isHolded = false;

        visual.anim.SetTrigger("LetGo");
    }

    void Move(Vector2 dir)
    {
        if (isHolded)
        {
            rb.velocity = Vector2.zero;

            return;
        }

        rb.AddForce(dir * accelSpeed);

        rb.velocity = Vector2.ClampMagnitude(rb.velocity, movementSpeed);
    }

    char FindFacingFromInput()
    {
        if (isHolded)
            return 'D';

        if (directionInput != Vector2.zero)
        {
            //Means he's going horizontally
            if (Mathf.Abs(directionInput.x) > Mathf.Abs(directionInput.y))
            {
                return directionInput.x > 0 ? 'R' : 'L';
            }
            //He is going vertically
            else
            {
                return directionInput.y > 0 ? 'U' : 'D';
            }
        }

        return 'D';
    }

    void OnTargetAchieved()
    {
        //Means we were chasing a chair, not a location or anythin...
        if (targetChair)
        {
            SitOnChair(targetChair);
        }
    }

    public void SitOnChair(Chair chair)
    {
        currChair = chair;

        targetChair = null;

        //It gets occupied
        currChair.GetOccupied(this);

        rb.bodyType = RigidbodyType2D.Static;
    }

    public void GoTo(Vector2 pos)
    {
        CancelChairFollow();

        if (currChair)
        {
            GetUpFromChair();
        }

        rb.bodyType = RigidbodyType2D.Dynamic;

        nodeFollower.Move(pos);
    }

    public void CancelChairFollow()
    {
        targetChair = null;

        currChair = null;

        if (nodeFollower)
        {
            nodeFollower.ClearMovement();
        }
    }

    public void GetUpFromChair()
    {
        if (currChair)
        {
            currChair.OnHumanLeft();
        }

        rb.bodyType = RigidbodyType2D.Dynamic;
        currChair = null;
    }

    public void ChaseChair(Chair chair)
    {
        if (nodeFollower)
        {
            nodeFollower.Move(chair.transform.position);

            targetChair = chair;

            currChair?.OnHumanLeft();

            currChair = null;

            chair.OnSetTarget(this);
        }
    }

    private void OnDrawGizmos()
    {
        if (!targetChair)
            return;

        Gizmos.color = Color.red;

        Gizmos.DrawLine(transform.position, targetChair.transform.position);

        Gizmos.DrawWireSphere(targetChair.transform.position, 0.4f);

    }
}
