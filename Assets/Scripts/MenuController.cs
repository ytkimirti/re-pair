﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public Transform mainTransform;
    public Transform[] slideTransforms;
    public int tutorialCount = 0;

    private Vector2 defaultPos;

    void Start()
    {
        //tutorialLength = Mathf.Abs(slideTransform.position.x);
        defaultPos = mainTransform.position;
        PositionScreens();
    }

    void PositionScreens()
    {
        for (int i = 0; i < slideTransforms.Length; i++)
        {
            slideTransforms[i].position = new Vector3((Screen.width / 2) - Screen.width * i, Screen.height / 2);
        }
    }

    // Update is called once per frame
    void Update()
    {
        float targetPos = Screen.width * tutorialCount + defaultPos.x;
        mainTransform.position = new Vector3(Mathf.Lerp(mainTransform.position.x, targetPos, 3F * Time.deltaTime), defaultPos.y);


        if (Input.anyKeyDown && tutorialCount != 0)
        {
            if (tutorialCount == 5)
            {
                Fader.main.FadeIn();
                Invoke("loadGame", 0.8F);
            }
            tutorialCount++;
        }
    }

    public void playGame()
    {
        tutorialCount = 1;
    }

    public void loadGame()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }
}
