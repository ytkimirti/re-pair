﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour
{
    public GameObject holdedGO;
    public Human holdedHuman;

    [Space]

    public float humanHoldOffset;

    [Space]

    public Sprite holdingSprite;
    public Sprite defSprite;

    [Space]

    public LayerMask chairLayers;
    public LayerMask holdableLayers;
    public float holdableRadius;

    [Space]

    public SpriteRenderer handRenderer;

    Camera cam;

    void Start()
    {
        cam = CameraController.main.cam;
    }

    void LateUpdate()
    {
        transform.position = (Vector2)cam.ScreenToWorldPoint(Input.mousePosition);

        GameObject currObject = CheckHand();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (currObject)
            {
                if (currObject.tag == "Human")
                {
                    holdedHuman = currObject.GetComponent<Human>();

                    if (holdedHuman.invincible)
                    {
                        holdedHuman = null;
                        currObject = null;

                        return;
                    }

                    holdedHuman.GetHolded();

                    holdedGO = currObject;
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            if (holdedHuman)
            {
                holdedHuman.GetDropped();

                //Check if there is a chair underneath
                Collider2D chairCol = Physics2D.OverlapCircle((Vector2)transform.position - Vector2.up * 0.5f, holdableRadius, chairLayers);

                if (chairCol)
                {
                    if (chairCol.gameObject.name == "Grinder")
                    {
                        if (holdedHuman)
                            holdedHuman.Die();
                    }
                    else
                    {

                        Chair chair = chairCol.transform.parent.gameObject.GetComponent<Chair>();

                        if (chair.currHuman)
                        {
                            chair.currHuman.transform.position = chair.currDesk.roadTrans.position;

                            chair.currHuman.GetUpFromChair();
                        }
                        else if (chair.targetHuman)
                        {
                            chair.targetHuman.CancelChairFollow();

                            chair.targetHuman = null;
                        }

                        holdedHuman.SitOnChair(chair);
                    }
                }

                holdedHuman = null;

                holdedGO = null;
            }
        }

        if (holdedGO)
        {
            Vector2 offset = Vector2.zero;

            if (holdedHuman)
            {
                offset = Vector2.up * humanHoldOffset;
            }

            holdedGO.transform.position = (Vector2)transform.position + offset;
        }

        //Visual


        handRenderer.sprite = holdedGO != null ? holdingSprite : defSprite;

    }

    GameObject CheckHand()
    {
        Collider2D col = Physics2D.OverlapCircle(transform.position, holdableRadius);

        if (col)
        {
            if (col.gameObject.tag == "HoldableCollider")
            {
                return col.gameObject.transform.parent.gameObject;
            }

            return col.gameObject;
        }

        return null;
    }
}
