﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Notif : MonoBehaviour
{
    public TextMeshPro notifText;
    public Animator anim;

    void Start()
    {

    }

    void Update()
    {

    }

    public void Notiffff(string text)
    {
        notifText.text = text;
        anim.SetTrigger("notif");
    }

    public void Notiffff(string text, Color color)
    {
        notifText.text = text;

        notifText.color = color;
        anim.SetTrigger("notif");
    }

    public void Notiffff(string text, Color color, bool bigNotif)
    {
        notifText.text = text;

        notifText.color = color;

        if (bigNotif)
        {
            anim.SetTrigger("notif_slow");
        }
        else
        {
            anim.SetTrigger("notif");
        }
    }
}
