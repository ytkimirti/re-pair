﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class DistractionInput : MonoBehaviour
{
    public string intruduceName;

    public float delay;

    public string eventName;

    public Transform[] pathTrans;

    public Vector2 nodeRandWait;

    public int jamAreaIDmin;
    public int jamAreaIDmax;

    public float shockwaveRadius;

    public float shockwaveDamage;

    public Animator shockwaveAnim;

    int currID;

    public Human human;

    bool intruduced = false;

    void Start()
    {
        StartCoroutine(moveLoop());
    }

    void Update()
    {

    }


    IEnumerator moveLoop()
    {
        yield return new WaitForSeconds(delay);

        while (true)
        {
            yield return new WaitForSeconds(Random.Range(nodeRandWait.x, nodeRandWait.y));

            if (!intruduced)
            {
                CameraShaker.Instance.ShakeOnce(1.5f, 7, 0, 0.5f);

                GameManager.main.bigNotif.Notiffff(intruduceName + " is here. \n Keep an eye on him hmmm...", Color.white, true);

                human.dialogBox.StartDialog(intruduceName);
                intruduced = true;
            }

            currID++;

            if (currID > jamAreaIDmin && currID < jamAreaIDmax)
            {
                human.dialogBox.StartDialog(GameManager.main.GetRandomDialog(eventName));

                yield return new WaitForSeconds(1);

                Shockwave();
            }

            if (currID >= pathTrans.Length)
            {
                currID = 0;
            }

            human.GoTo(pathTrans[currID].position);

        }
    }

    public void Shockwave()
    {
        shockwaveAnim.SetTrigger("shockwave");

        CameraShaker.Instance.ShakeOnce(2f, 7, 0, 0.8f);

        GameManager.main.Shockwave(shockwaveRadius, shockwaveDamage, transform.position, "disteraction");
    }
}
