﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoneManager : MonoBehaviour
{
    public Transform jamZone;
    public Transform disZone;
    public Transform doorZone;

    [Header("Pathfinding Nodes")]
    public Transform jamZoneCenter;

    public Transform doorBottomNode;
    public Transform doorJamNode;

    public static ZoneManager main;

    void Awake()
    {
        main = this;
    }

    void Start()
    {

    }

    //Checks if the given position is inside the given zone. Uses scale property of the transform
    bool ContainsPos(Vector2 pos, Transform zoneTrans)
    {
        Vector2 center = zoneTrans.position;
        Vector2 scale = zoneTrans.localScale / 2;//Local scale means the width of the object, we only want the radius

        if (pos.x < center.x + scale.x && pos.x > center.x - scale.x && pos.y < center.y + scale.y && pos.y > center.y - scale.y)
        {
            return true;
        }

        return false;
    }

    public string WhichZone(Vector2 pos)
    {
        if (ContainsPos(pos, jamZone))
        {
            return "jam";
        }
        else if (ContainsPos(pos, disZone))
        {
            return "dis";
        }
        else if (ContainsPos(pos, doorZone))
        {
            return "door";
        }

        return "null";
    }

    Vector2 NearestJamZoneCenterPoint(Vector2 pos)
    {
        return new Vector2(pos.x, jamZoneCenter.transform.position.y);
    }

    public Vector2[] FindPath(Vector2 pos, Vector2 target)
    {
        string zoneA = WhichZone(pos);
        string zoneB = WhichZone(target);

        List<Vector2> path = new List<Vector2>();

        if (zoneA == zoneB)
        {
            if (zoneA == "jam")
            {
                path.Add(NearestJamZoneCenterPoint(pos));
                path.Add(NearestJamZoneCenterPoint(target));
                //path.Add(target);
            }
            else
            {
                //path.Add(target);
            }
        }
        else if ((zoneA == "door" && zoneB == "dis") || (zoneA == "dis" && zoneB == "door"))
        {
            path.Add(doorBottomNode.position);
            //path.Add(target);
        }
        else if (zoneA == "jam" && zoneB == "door")
        {
            path.Add(NearestJamZoneCenterPoint(pos));
            path.Add(doorJamNode.position);
            //path.Add(target);
        }
        else if (zoneA == "door" && zoneB == "jam")
        {
            path.Add(doorJamNode.position);
            path.Add(NearestJamZoneCenterPoint(target));
            //path.Add(target);
        }
        else if (zoneA == "jam" && zoneB == "door")
        {
            path.Add(NearestJamZoneCenterPoint(pos));
            path.Add(doorJamNode.position);
            //path.Add(target);
        }
        else if (zoneA == "jam" && zoneB == "dis")
        {
            path.Add(NearestJamZoneCenterPoint(pos));
            path.Add(doorJamNode.position);
            path.Add(doorBottomNode.position);
            //path.Add(target);
        }
        else if (zoneA == "dis" && zoneB == "jam")
        {
            path.Add(doorBottomNode.position);
            path.Add(doorJamNode.position);
            path.Add(NearestJamZoneCenterPoint(pos));
            //path.Add(target);
        }

        path.Add(target);

        return path.ToArray();
    }

    void Update()
    {

    }
}
