﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chair : MonoBehaviour
{
    public string status = "open";
    [Space]
    public Desk currDesk;
    [Space]
    //The current SITTING human
    public Human currHuman;

    //The current human thats COMING FOR THIS SEAT
    public Human targetHuman;

    [Space]
    public char currFacing = 'R';

    [Header("References")]

    public Transform visualsTrans;

    private void OnValidate()
    {
        UpdateFacing();
    }

    void UpdateFacing()
    {
        if (currFacing == 'R')
        {
            visualsTrans.localScale = Vector3.one;
        }
        else
        {
            visualsTrans.localScale = new Vector3(-1, 1, 1);
        }
    }

    public void OnSetTarget(Human human)
    {
        targetHuman = human;

        status = "waiting_for_human";
    }

    public void GetOccupied(Human human)
    {
        currHuman = human;

        targetHuman = null;

        human.transform.position = transform.position;
    }

    public void OnHumanLeft()
    {
        currHuman = null;
    }

    void LookForDesk()
    {
        currDesk = GetComponentInParent<Desk>();

    }

    void Start()
    {
        LookForDesk();

        UpdateFacing();
    }

    void Update()
    {

        //If there is someone coming for this chair (at least the variable says so) and that man is actually not coming for
        //this chair, fix that property. (They probably died or got picked up by player).
        if ((targetHuman && targetHuman.targetChair != this))
        {
            targetHuman = null;
        }
        else if (currHuman && currHuman.currChair != this)
        {
            currHuman = null;
        }

        status = GetStatus();
    }

    string GetStatus()
    {
        if (targetHuman)
        {
            return "waiting_for_human";
        }
        else if (currHuman)
        {
            return "occupied";
        }
        else
        {
            return "open";
        }
    }
}
