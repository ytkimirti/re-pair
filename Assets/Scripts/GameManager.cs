﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using NaughtyAttributes;
using EZCameraShake;

[System.Serializable]
public class EventDialog
{
    public string name;

    public string[] dialoges;

    public string RandomDialog()
    {
        if (dialoges.Length > 0)
        {
            return dialoges[Random.Range(0, dialoges.Length)];
        }

        return "";
    }
}

public class GameManager : MonoBehaviour
{
    Chair[] allChairs;

    bool isGameOver = false;

    [Header("References")]

    public GameObject jammerPrefab;
    public Transform spawnTrans;

    [Header("Random event")]

    public Vector2 randomEventTime;

    [Header("DIALOGES")]

    public EventDialog[] eventDialoges;

    [Header("ALL THE SPRITES")]

    public SpritePart[] bodyParts;
    public SpritePart[] handParts;
    public SpritePart[] footParts;
    public SpritePart[] hairParts;

    public FacialExp[] facialExps;

    [Header("Other")]

    public Clock clock;

    public GameObject hand;

    public Animator gameOverAnim;

    public Color artistColor, developerColor, musicColor;

    public Notif bigNotif;

    public static GameManager main;

    private void Awake()
    {
        main = this;
    }

    public string GetRandomDialog(string eventName)
    {
        foreach (EventDialog dialog in eventDialoges)
        {
            if (eventName == dialog.name)
            {
                return dialog.RandomDialog();
            }
        }

        return "";
    }

    void Start()
    {
        StartCoroutine("GameEnum");

        allChairs = FindObjectsOfType<Chair>();

        SpawnOneJammerOfEveryRole(4, spawnTrans.position);
    }

    public void SpawnOneJammerOfEveryRole(int count, Vector2 pos)
    {
        SpawnHuman(jammerPrefab, Role.Developer, count);
        SpawnHuman(jammerPrefab, Role.Artist, count);
        SpawnHuman(jammerPrefab, Role.Music, count);
    }

    public void SpawnHuman(GameObject prefab, Role role = Role.None, int count = 1)
    {
        for (int i = 0; i < count; i++)
        {
            GameObject humanGo = Instantiate(prefab, RandomSpawnPoint(), Quaternion.identity);

            Human human = humanGo.GetComponent<Human>();

            human.role = role;
        }
    }

    IEnumerator GameEnum()
    {
        yield return new WaitForSeconds(Random.Range(randomEventTime.x, randomEventTime.y));

        RandomGlobalEvent();
    }

    public void Shockwave(float radius, float damage, Vector2 pos, string message)
    {
        JammerInput[] jammers = FindObjectsOfType<JammerInput>();

        foreach (JammerInput jammer in jammers)
        {
            if (Vector2.Distance(jammer.transform.position, pos) < radius)
            {
                jammer.human.ChangeStamina(damage, message);
            }
        }
    }

    [Button]
    public void RandomGlobalEvent()
    {
        GlobalEnumEvent("internet_gone");
    }

    public void GlobalEnumEvent(string enumName)
    {
        JammerInput[] jammers = FindObjectsOfType<JammerInput>();

        if (enumName == "internet_gone")
        {
            bigNotif.Notiffff("The internet is gone! \n Re-pair them...", Color.red, true);
        }

        foreach (JammerInput jammer in jammers)
        {
            jammer.CororoutineStart(enumName);
        }
    }

    public Chair FindAvailableChair()
    {
        foreach (Chair chair in allChairs)
        {
            if (chair.status == "open")
            {
                return chair;
            }
        }

        return null;
    }

    public Vector2 RandomSpawnPoint()
    {
        Vector2 pointScale = spawnTrans.localScale / 2;

        return new Vector2(Random.Range(spawnTrans.position.x - pointScale.x, spawnTrans.position.x + pointScale.x)
        , Random.Range(spawnTrans.position.y - pointScale.y, spawnTrans.position.y + pointScale.y));
    }

    public Chair FindAvailableRandomChair()
    {
        int failCount = 0;

        Chair currChair = allChairs[Random.Range(0, allChairs.Length)];

        while (currChair.status != "open" && failCount < 50)
        {
            failCount++;

            currChair = allChairs[Random.Range(0, allChairs.Length)];
        }

        return currChair;
    }

    public void GameOver()
    {
        if (isGameOver)
        {
            return;
        }

        hand.SetActive(false);

        isGameOver = true;

        gameOverAnim.SetTrigger("gameover");
    }

    public void OnDeskCycle()
    {
        CameraShaker.Instance.ShakeOnce(2, 10, 0, 0.2f);
    }

    // Update is called once per frame
    void Update()
    {
        if (clock.currTime > 48)
        {
            GameOver();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Fader.main.FadeIn();

            Invoke("LoadMenu", Fader.main.fadeSpeed);
        }
    }

    void LoadMenu()
    {
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
